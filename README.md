# Introduction

This project creates a docker image with JRE 10 installed installed on Debian Buster.

The image can be used to run applications using java.

This repository is mirrored to https://gitlab.com/sw4j-net/jre10

## Deprecation

As Java 10 is deprecated this repository and the generated images will be removed in January 2019.
